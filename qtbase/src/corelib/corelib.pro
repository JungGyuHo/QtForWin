TARGET     = QtCore
QT         =
CONFIG    += exceptions

MODULE = core     # not corelib, as per project file
MODULE_CONFIG = moc resources
!isEmpty(QT_NAMESPACE): MODULE_DEFINES = QT_NAMESPACE=$$QT_NAMESPACE

CONFIG += $$MODULE_CONFIG
DEFINES += $$MODULE_DEFINES
DEFINES   += QT_NO_USING_NAMESPACE
win32-msvc*|win32-icc:QMAKE_LFLAGS += /BASE:0x67000000
irix-cc*:QMAKE_CXXFLAGS += -no_prelink -ptused

CONFIG += optimize_full

QMAKE_DOCS = $$PWD/doc/qtcore.qdocconf

ANDROID_JAR_DEPENDENCIES = \
    jar/QtAndroid.jar \
    jar/QtAndroidAccessibility.jar
ANDROID_LIB_DEPENDENCIES = \
    plugins/platforms/android/libqtforandroid.so
ANDROID_BUNDLED_JAR_DEPENDENCIES = \
    jar/QtAndroid-bundled.jar \
    jar/QtAndroidAccessibility-bundled.jar
ANDROID_PERMISSIONS = \
    android.permission.INTERNET \
    android.permission.WRITE_EXTERNAL_STORAGE

# QtCore can't be compiled with -Wl,-no-undefined because it uses the "environ"
# variable and on FreeBSD and OpenBSD, this variable is in the final executable itself.
# OpenBSD 6.0 will include environ in libc.
freebsd|openbsd: QMAKE_LFLAGS_NOUNDEF =

load(qfeatures)

include(animation/animation.pri)
include(arch/arch.pri)
include(global/global.pri)
include(thread/thread.pri)
include(tools/tools.pri)
include(io/io.pri)
include(itemmodels/itemmodels.pri)
include(json/json.pri)
include(plugin/plugin.pri)
include(kernel/kernel.pri)
include(codecs/codecs.pri)
include(statemachine/statemachine.pri)
include(mimetypes/mimetypes.pri)
include(xml/xml.pri)

# otherwise mingw headers do not declare common functions like putenv
mingw: CONFIG -= strict_c++

mac|darwin {
    !ios {
        LIBS_PRIVATE += -framework ApplicationServices
        LIBS_PRIVATE += -framework CoreServices
    }
    LIBS_PRIVATE += -framework CoreFoundation
    LIBS_PRIVATE += -framework Foundation
}

QMAKE_LIBS += $$QMAKE_LIBS_CORE

QMAKE_DYNAMIC_LIST_FILE = $$PWD/QtCore.dynlist

contains(DEFINES,QT_EVAL):include(eval.pri)

HOST_BINS = $$[QT_HOST_BINS]
host_bins.name = host_bins
host_bins.variable = HOST_BINS

qt_conf.name = qt_config
qt_conf.variable = QT_CONFIG

QMAKE_PKGCONFIG_VARIABLES += host_bins qt_conf

load(qt_module)

# Override qt_module, so the symbols are actually included into the library.
win32: DEFINES -= QT_NO_CAST_TO_ASCII
