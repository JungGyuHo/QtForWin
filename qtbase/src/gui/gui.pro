TARGET     = QtGui
QT = core core-private
MODULE_CONFIG = uic
contains(QT_CONFIG, opengl.*):MODULE_CONFIG += opengl

CONFIG += $$MODULE_CONFIG
DEFINES   += QT_BUILD_GUI_LIB QT_NO_USING_NAMESPACE

QMAKE_DOCS = $$PWD/doc/qtgui.qdocconf

MODULE_PLUGIN_TYPES = \
    platforms \
    xcbglintegrations \
    platformthemes \
    platforminputcontexts \
    generic \
    iconengines \
    imageformats \
    egldeviceintegrations

# This is here only because the platform plugin is no module, obviously.
win32:contains(QT_CONFIG, angle)|contains(QT_CONFIG, dynamicgl) {
    MODULE_AUX_INCLUDES = \
        \$\$QT_MODULE_INCLUDE_BASE/QtANGLE
}

# Code coverage with TestCocoon
# The following is required as extra compilers use $$QMAKE_CXX instead of $(CXX).
# Without this, testcocoon.prf is read only after $$QMAKE_CXX is used by the
# extra compilers.
testcocoon {
    load(testcocoon)
}

mac:!ios: LIBS_PRIVATE += -framework Cocoa

CONFIG += simd optimize_full

#platforms
x11:include(kernel/x11.pri)
mac:include(kernel/mac.pri)
win32:include(kernel/win.pri)
embedded:include(embedded/embedded.pri)

include(animation/animation.pri)
include(kernel/kernel.pri)
include(image/image.pri)
include(painting/painting.pri)
include(text/text.pri)
include(styles/styles.pri)
include(widgets/widgets.pri)
include(dialogs/dialogs.pri)
include(accessible/accessible.pri)
include(itemmodels/itemmodels.pri)
include(itemviews/itemviews.pri)
include(inputmethod/inputmethod.pri)
include(graphicsview/graphicsview.pri)
include(util/util.pri)
include(statemachine/statemachine.pri)
include(math3d/math3d.pri)
include(effects/effects.pri)

include(egl/egl.pri)
win32:!wince*: DEFINES += QT_NO_EGL
embedded: QT += network

QMAKE_LIBS += $$QMAKE_LIBS_GUI

contains(DEFINES,QT_EVAL):include($$QT_SOURCE_TREE/src/corelib/eval.pri)

DEFINES += Q_INTERNAL_QAPP_SRC


win32:!contains(QT_CONFIG, directwrite) {
    DEFINES += QT_NO_DIRECTWRITE
}

MODULE_PLUGIN_TYPES += \
    styles
load(qt_module)

QMAKE_DYNAMIC_LIST_FILE = $$PWD/QtGui.dynlist
