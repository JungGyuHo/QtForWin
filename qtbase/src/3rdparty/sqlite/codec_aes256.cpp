#ifdef SQLITE_HAS_CODEC

#include "codec.h"
#include "aes256.hpp"

#include <vector>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef AES256_KEY_SIZE 
#define AES256_KEY_SIZE 32
#endif

typedef struct tagST_CODEC_HANDLE
{
    AES256*                                         object;
} ST_CODEC_HANDLE, *LPST_CODEC_HANDLE;

HANDLE_CODEC SQLiteAES256Init(IN_PARAM const void* pKey, IN_PARAM int nCbKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize)
{
	void*				pRtnValue		= NULL;
	LPST_CODEC_HANDLE	pstCodecHandle	= NULL;

    do 
    {
        if( pKey == NULL || nCbKey <= 0 )
        {
            assert( 0 );
            break;
        }

        std::string key;
        for( int idx = 0; idx < nCbKey; ++idx )
            key.push_back( ( (char*)pKey )[ idx ] );

        char szBuffer[ 4 ] = { 0, };
        char* pszBuffer = NULL;
        szBuffer[ 0 ] = ( (char*)pKey )[ 0 ];
        szBuffer[ 1 ] = ( (char*)pKey )[ 1 ];
        szBuffer[ 2 ] = ( (char*)pKey )[ 2 ];

        pszBuffer = _strlwr( szBuffer );
        if( pszBuffer[ 0 ] == 'a' && pszBuffer[ 1 ] == '2' && pszBuffer[ 2 ] == ':' )
        {
            // erase "a2:" string 
            key.erase( key.begin() );
            key.erase( key.begin() );
            key.erase( key.begin() );
        }
        else
        {
            assert( 0 );
            break;
        }

        if( key.size() > AES256_KEY_SIZE )
            key.resize( AES256_KEY_SIZE );

        if( key.size() < AES256_KEY_SIZE )
        {
            int currentSize = key.size();
            key.resize( AES256_KEY_SIZE );
            for( int idx = currentSize; idx < AES256_KEY_SIZE; ++idx )
            {
                key[ idx ] =  idx % 2 == 0 ? 'a' : 'z';
            }
        }

        pstCodecHandle = new ST_CODEC_HANDLE;
        if( pstCodecHandle == NULL )
        {
            assert( 0 );
            break;
        }

        memset( pstCodecHandle, '\0', sizeof( ST_CODEC_HANDLE ) );
        pstCodecHandle->object = new AES256( key, AES256::Chaining_Mode::CBC );
        
        // AES 256 block 크기
        // 즉, AES 256는 단순히 비어있는 문자열 ""를 암호화해도 16byte 크기로 암호화된다.
        // 여기는 그 값을 넣으면 된다.
        *punCbBlockSize = 16;

        pRtnValue = pstCodecHandle;


    } while (false);

    return pRtnValue;
}

void SQLiteAES256DeInit(IN_PARAM HANDLE_CODEC hHandle)
{
	LPST_CODEC_HANDLE pstHandle = NULL;

    do 
    {
        pstHandle = (LPST_CODEC_HANDLE)hHandle;
        if( pstHandle == NULL )
            break;

        delete hHandle;
        hHandle = NULL;

    } while (false);
}

int SQLiteAES256Encode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest)
{
	int					nRtnValue	= 1;
	LPST_CODEC_HANDLE	pstHandle	= NULL;

    do 
    {
        *pnCbDest = 0;
        pstHandle = (LPST_CODEC_HANDLE)hHandle;
        if( pstHandle == NULL || pstHandle->object == NULL )
        {
            nRtnValue = 0;
            assert( 0 );
            break;
        }
        
        std::string enc = pstHandle->object->encrypt( std::string( (char*)pSource, nCbSource ), true );
        assert( enc.size() <= nCbDestBufSize );
#if defined( _MSC_VER )
        if( enc.size() > nCbDestBufSize )
            __debugbreak();
#endif

        memcpy( pDest, &enc[0], enc.size() );
        *pnCbDest = enc.size();

    } while (false);

	return *pnCbDest;
}

int SQLiteAES256Decode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest)
{
	int					nRtnValue	= 1;
	LPST_CODEC_HANDLE	pstHandle	= NULL;

    do 
    {
        *pnCbDest = 0;
        pstHandle = (LPST_CODEC_HANDLE)hHandle;
        if( pstHandle == NULL || pstHandle->object == NULL )
        {
            nRtnValue = 0;
            assert( 0 );
            break;
        }

        std::string dec = pstHandle->object->decrypt( std::string( (char*)pSource, nCbSource ), true );
        assert( dec.size() <= nCbDestBufSize );
#if defined( _MSC_VER )
        if( dec.size() > nCbDestBufSize )
            __debugbreak();
#endif
        memcpy( pDest, &dec[ 0 ], dec.size() );
        *pnCbDest = dec.size();

    } while (false);

	return *pnCbDest;
}

#ifdef __cplusplus
}
#endif

#endif

