#ifdef SQLITE_HAS_CODEC

#include <memory.h>

#include "codec.h"
#include "blowfish.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tagST_CODEC_HANDLE
{
	// Blowfish instance가 들어간다.
	Blowfish *pBF;
} ST_CODEC_HANDLE, *LPST_CODEC_HANDLE;

HANDLE_CODEC SQLiteBlowfishInit(IN_PARAM const void* pKey, IN_PARAM int nCbKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize)
{
	void*				pRtnValue		= NULL;
	LPST_CODEC_HANDLE	pstCodecHandle	= NULL;
	char*				lpszKey			= NULL;

    do 
    {
        if( pKey == NULL || nCbKey <= 0 )
        {
            assert( 0 );
            break;
        }

        pstCodecHandle = new ST_CODEC_HANDLE;
        if( pstCodecHandle == NULL )
        {
            assert( 0 );
            break;
        }

        memset( pstCodecHandle, '\0', sizeof( ST_CODEC_HANDLE ) );

        lpszKey = new char[ nCbKey + 1 ];
        if( lpszKey == NULL )
        {
            assert( 0 );
            break;
        }

        memset( lpszKey, '\0', sizeof( char )*( nCbKey + 1 ) );

#if defined( _MSC_VER )
        strncpy_s( lpszKey, nCbKey + 1, (char*)pKey, _TRUNCATE );
#else
        strncpy( lpszKey, (char*)pKey, nCbKey + 1 );
#endif

        // Blowfish instance를 만들고, password 세팅한다.
        pstCodecHandle->pBF = new Blowfish;

        char szBuffer[ 4 ] = { 0, };
        char* pszBuffer = NULL;
        szBuffer[ 0 ] = ((char*)pKey)[ 0 ];
        szBuffer[ 1 ] = ((char*)pKey)[ 1 ];
        szBuffer[ 2 ] = ((char*)pKey)[ 2 ];

        pszBuffer = _strlwr( szBuffer );
        if( pszBuffer[ 0 ] == 'b' && pszBuffer[ 1 ] == 'f' && pszBuffer[ 2 ] == ':' )
            pstCodecHandle->pBF->Set_Passwd( &lpszKey[3] );
        else
            pstCodecHandle->pBF->Set_Passwd( lpszKey );

        // blowfish block 크기
        // 알고리즘 특징상 input size와 output size가 같다.
        // 단, size는 8의 배수배 단위여야 한다.
        // (sqlite의 page 단위가 2^n이고, 512보다 크므로, 8byte 단위임이 확실하기 때문이다.)
        *punCbBlockSize = 0;

        pRtnValue = pstCodecHandle;

    } while (false);

	if( lpszKey != NULL )
	{
		delete [] lpszKey;
		lpszKey = NULL;
	}

	return pRtnValue;
}

void SQLiteBlowfishDeInit(IN_PARAM HANDLE_CODEC hHandle)
{
	LPST_CODEC_HANDLE pstHandle = NULL;

    do 
    {
        pstHandle = (LPST_CODEC_HANDLE)hHandle;
        if( pstHandle == NULL )
            break;

        if( pstHandle->pBF != NULL )
        {
            delete pstHandle->pBF;
            pstHandle->pBF = NULL;
        }

        delete hHandle;
        hHandle = NULL;

    } while (false);
}

int SQLiteBlowfishEncode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest)
{
	int					nRtnValue	= 1;
	LPST_CODEC_HANDLE	pstHandle	= NULL;
	unsigned int		dwCbLength	= 0;

	pstHandle = (LPST_CODEC_HANDLE)hHandle;

    do 
    {
        if( pstHandle == NULL || pstHandle->pBF == NULL )
        {
            nRtnValue = 0;
            assert( 0 );
            break;
        }

        if( 0 == nCbSource % 8 )
        {
            // good
        }
        else
        {
            // bad
            // blowfish는 8byte 단위가 들어와야 한다.
            assert( 0 );
            break;
        }

        // 암호화!
        dwCbLength = nCbSource;
        memcpy( pDest, pSource, nCbSource );
        pstHandle->pBF->Encrypt( pDest, dwCbLength );
        *pnCbDest = dwCbLength;

    } while (false);

    return nRtnValue;
}

int SQLiteBlowfishDecode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest)
{
	int					nRtnValue	= 1;
	LPST_CODEC_HANDLE	pstHandle	= NULL;
	unsigned int     	dwCbLength	= 0;

    do 
    {
        pstHandle = (LPST_CODEC_HANDLE)hHandle;
        if( pstHandle == NULL || pstHandle->pBF == NULL )
        {
            nRtnValue = 0;
            assert( 0 );
            break;
        }

        if( 0 == nCbSource % 8 )
        {
            // good
        }
        else
        {
            // bad
            // blowfish는 8byte 단위가 들어와야 한다.
            assert( 0 );
            break;
        }

        // 복호화!
        dwCbLength = nCbSource;
        memcpy( pDest, pSource, nCbSource );
        pstHandle->pBF->Decrypt( pDest, dwCbLength );
        *pnCbDest = dwCbLength;

    } while (false);

	return nRtnValue;
}

#ifdef __cplusplus
}
#endif

#endif

