﻿#ifndef HEADER_CODEC_H
#define HEADER_CODEC_H

#include "sqliteInt.h"
#include "btreeInt.h"

#if defined(_MSC_VER)
#   pragma execution_character_set( "utf-8" )
#endif

/*!

    흐름

    sqlite3CodecAttach 호출
        ATTACH DATABASE db AS dba KEY k
        PRAGMA KEY=k

        #. CodecInternal, CodecFreeInternal 설정
        #. SQLiteCodecInit 함수를 호출하여 코덱 초기화


    codecInternal 에서 암호화 수행

    based on botansqlite3
*/

//////////////////////////////////////////////////////////////////////////
// License
//
// SQLiteCodecWrapper.
// jun jin pyo.
//
// http://greenfishblog.tistory.com/134
//
// This source code is under CC license.(http://creativecommons.org/licenses/)
// And any change of this source code is NOT permitted.
// BY-NC-ND was applied.
//
// 1. Redistributions of source code must retain the above copywrite notice with
//    no modification.
//
// 2. The use of this is non-commercial.
//
//////////////////////////////////////////////////////////////////////////

// parameter from sqlite tp user function
#define IN_PARAM

// parameter from user function to sqlite
#define OUT_PARAM

// parameter from user function to sqlite. you can ignore it.
#define OUT_OPTINAL_PARAM

// context for codec
typedef void* HANDLE_CODEC;

// declare sqlite codec function pointer
typedef HANDLE_CODEC (*PFN_SQLiteCodecInit)( IN_PARAM const void* pKey, IN_PARAM int nCbKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize );
typedef void (*PFN_SQLiteCodecDeInit)( IN_PARAM HANDLE_CODEC hHandle );
typedef int (*PFN_SQLiteCodecEncode)( IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest );
typedef int (*PFN_SQLiteCodecDecode)( IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest );

typedef enum
{
    SQLITE_CODEC_NONE,
    SQLITE_CODEC_XOR,
    SQLITE_CODEC_BLOWFISH,
    SQLITE_CODEC_AES256

} TyEnSQLiteEncryptionType;

typedef struct tagCODEC_CONTEXT
{
    TyEnSQLiteEncryptionType                        eEncryptionType;

    unsigned int                                    unBlockSize;
    unsigned int                                    unPageSize;
    unsigned int                                    unReserveSize;

    // HANDLE_CODEC
    HANDLE_CODEC                                    handle;

    struct Db*                                      pDB;
    Btree*                                          pBt;
    void*                                           pBuffer;
    int                                             nBufferSize;

    PFN_SQLiteCodecInit                             pfnSQLiteCodecInit;
    PFN_SQLiteCodecDeInit                           pfnSQLiteCodecDeInit;
    PFN_SQLiteCodecEncode                           pfnSQLiteCodecEncode;
    PFN_SQLiteCodecDecode                           pfnSQLiteCodecDecode;

} CODEC_CONTEXT, *PCODEC_CONTEXT;

TyEnSQLiteEncryptionType    GetSQLiteEncryptionType( const void* zKey, int nKey );
void                        SetSQLiteEncryptionHandler( PCODEC_CONTEXT pContext );
int                         sqlite3CodecAttach( sqlite3* db, int nDb, const void* zKey, int nKey );

#ifdef __cplusplus
extern "C" {
#endif

    HANDLE_CODEC         SQLiteXORInit( IN_PARAM const void* zKey, IN_PARAM int nKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize );
    void                 SQLiteXORDeInit( IN_PARAM HANDLE_CODEC handle );
    int                  SQLiteXOREncode( IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest );
    int                  SQLiteXORDecode( IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest );

    HANDLE_CODEC         SQLiteBlowfishInit( IN_PARAM const void* zKey, IN_PARAM int nKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize );
    void                 SQLiteBlowfishDeInit( IN_PARAM HANDLE_CODEC handle );
    int                  SQLiteBlowfishEncode( IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest );
    int                  SQLiteBlowfishDecode( IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest );

    HANDLE_CODEC         SQLiteAES256Init( IN_PARAM const void* zKey, IN_PARAM int nKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize );
    void                 SQLiteAES256DeInit( IN_PARAM HANDLE_CODEC handle );
    int                  SQLiteAES256Encode( IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest );
    int                  SQLiteAES256Decode( IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest );

#ifdef __cplusplus
}
#endif

#endif // HEADER_CODEC_H
