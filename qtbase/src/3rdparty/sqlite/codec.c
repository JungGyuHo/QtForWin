//////////////////////////////////////////////////////////////////////////
// License
//
// SQLiteCodecWrapper.
// jun jin pyo.
//
// http://greenfishblog.tistory.com/134
//
// This source code is under CC license.(http://creativecommons.org/licenses/)
// And any change of this source code is NOT permitted.
// BY-NC-ND was applied.
//
// 1. Redistributions of source code must retain the above copywrite notice with
//    no modification.
//
// 2. The use of this is non-commercial.
//
//////////////////////////////////////////////////////////////////////////

// build when SQLITE_HAS_CODEC was defined
#ifdef SQLITE_HAS_CODEC

#include "codec.h"
#include "pager.h"
#include <assert.h>

/*
    Description

    1)
    To build successfully, you define following four functions in your project.
    They are referenced from the sqlite3 codes as c function,
    so if you are planed to define them in c++, they must be in __cplusplus.
    ----------------------
    | #ifdef __cplusplus |
    | extern "C" {       |
    | #endif             |
    | ...                |
    | #ifdef __cplusplus |
    | }                  |
    | #endif             |
    ----------------------

    2) do not use amalgamation version of sqlite.
    preprocessed version of legacy source code is recommended.

    HANDLE_CODEC SQLiteCodecInit(IN_PARAM const void* pKey, IN_PARAM int nCbKey, OUT_OPTINAL_PARAM unsigned int* punCbBlockSize);
    pKey            : Key

    nCbKey          : Key length (in bytes)

    punCbBlockSize  : block size for encrypt.

    if you'll use some symmetric cryptosystem(ex, AES 256),
    some of them has a fixed increasing size when encrypt.
    you must assign that fixed increasing size.
    you can easily get it simply as encrypt ""(empty data).
    the size of encrypt data can be considered.

    if you using simply XOR encoding, and that encoding
    does not have increasing size. so you can ignore this parameter.

    [return] Success : Context pointer / Fail : NULL

    it is called when "PRAGMA KEY='...'" was executed or sqlite3_key(...) was called.
    you must return HANDLE(or void*).
    that value can be used as a context of processing codec.

    if NULL was returned, the call of 'PRAGMA KEY=' or sqlite3_key(...) was FAILED as SQLITE_NOMEM.
    so the initialization of codec module was failed, you can pass NULL.

    void SQLiteCodecDeInit(IN_PARAM HANDLE_CODEC hHandle);
    hHandle : HANDLE of SQLiteCodecInit(...)

    it is called when db was about closed.
    you can clean up all context from SQLiteCodecInit(...)

    int SQLiteCodecEncode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest);
    int SQLiteCodecDecode(IN_PARAM HANDLE_CODEC hHandle, IN_PARAM void* pSource, IN_PARAM int nCbSource, OUT_PARAM void* pDest, IN_PARAM int nCbDestBufSize, OUT_PARAM int* pnCbDest);
    hHandle        : HANDLE of SQLiteCodecInit(...)

    pSource        : En(De)crypt Source

    nCbSource      : pSource size (in bytes)

    pDest          : De(En)crypt Destination which must be filled in.

    nCbDestBufSize : pDest allocation size (in bytes)

    pnCbDest       : De(En)crypted size (in bytes)

    [return] Success : 1 / Fail : 0

    it is called when the database action was executed.

    if en(de)cryption was failed, you can pass return as 0.
    when it was returned as 0, the database action was failed.
    you can pass return as 1, it is that en(de)cryption was successful.

    [remark]
    pDest was guaranteed to be zero memory.
*/

// reference function in pager.c
extern void sqlite3PagerSetCodec( Pager *pPager, void *( *xCodec )( void*, void*, Pgno, int ), void( *xCodecSizeChng )( void*, int, int ), void( *xCodecFree )( void* ), void *pCodec );
extern void* sqlite3PagerGetCodec( Pager *pPager );

// PRAGMA KEY='xxxx';
int sqlite3_key( sqlite3 *db, const void *pKey, int nKey )
{
    if( ( 0 == nKey ) || ( NULL == pKey ) )
    {
        return SQLITE_OK;
    }

    return sqlite3CodecAttach( db, 0, pKey, nKey );
}

int sqlite3_key_v2(
    sqlite3 *db,                   /* Database to be rekeyed */
    const char *zDbName,           /* Name of the database */
    const void *pKey, int nKey     /* The key */
)
{
    if( ( 0 == nKey ) || ( NULL == pKey ) )
    {
        return SQLITE_OK;
    }

    return sqlite3CodecAttach( db, 0, pKey, nKey );
}

// change key. not implemented.
// you can change key to call sqlite3_backup_init.
int sqlite3_rekey( sqlite3 *db, const void *pKey, int nKey )
{
    assert( 0 );
    return SQLITE_ERROR;
}

int sqlite3_rekey_v2(
    sqlite3 *db,                   /* Database to be rekeyed */
    const char *zDbName,           /* Name of the database */
    const void *pKey, int nKey     /* The new key */
)
{
    (void)( zDbName );
    return sqlite3_rekey( db, pKey, nKey );
}

// called from
// "vaccum;" (nDB = 0)
// "attach database 'xxx.db' as db2"; (nDB > 0)
void sqlite3CodecGetKey( sqlite3* db, int nDb, void** zKey, int* nKey )
{
    // The unencrypted password is not stored for security reasons
    // therefore always return NULL
    *zKey = NULL;
    *nKey = 928;
}

// Guessing that "see" is related to SQLite Encryption Extension" (the semi-official, for-pay, encryption codec)
// Just as useful for initializing Library
void sqlite3_activate_see( const char *zPassPhrase )
{
    return;
}

// Encrypt/Decrypt functionality, called by pager.c
void* sqlite3Codec( void* pContext, void* data, Pgno nPageNum, int nMode )
{
    PCODEC_CONTEXT              pstContext = NULL;
    unsigned char*              pRtnValue = NULL;
    int                         nDataSize = 0;
    int                         nPageSize = 0;
    int                         nReserveSize = 0;

    int                         i = 0;
    int                         nCbCodec = 0;
    int                         nCbBuffer = 0;
    int                         rc = SQLITE_OK;

    do
    {
        pstContext = (PCODEC_CONTEXT)pContext;
        if( pstContext == NULL )
            break;

        if( pstContext->handle == NULL )
            break;

        // get page and reserve size
        nPageSize = sqlite3BtreeGetPageSize( pstContext->pBt );
        nReserveSize = sqlite3BtreeGetOptimalReserve( pstContext->pBt );

        // actual size to be saved
        nDataSize = nPageSize - nReserveSize;
        if( nDataSize < 0 )
        {
            assert( 0 );
            break;
        }

        switch( nMode )
        {
            //////////////////////////////////////////////////////////////////////////
            // decrypt
            case 0: // journal file decrypt
            case 2: // page reload
            case 3: // page load
            {
                // decode here
                if( 0 == pstContext->pfnSQLiteCodecDecode( pstContext->handle,
                    data,
                    nPageSize,
                    pstContext->pBuffer,
                    pstContext->nBufferSize,
                    &nCbBuffer ) )
                {
                    // return as NULL.
                    // execute of database will be failed.
                    assert( 0 );
                    break;
                }

                if( nCbBuffer != nDataSize )
                {
                    // must be same
                    assert( 0 );
                    break;
                }
                else
                {
                    pRtnValue = pstContext->pBuffer;

                    // save to data
                    memcpy( data, pstContext->pBuffer, nDataSize );
                }
            }
            break;

            //////////////////////////////////////////////////////////////////////////
            // encrypt
            case 6: // page encrypt
            case 7: // journal file encrypt
            {
                // encrypt here
                if( 0 == pstContext->pfnSQLiteCodecEncode( pstContext->handle,
                    data,
                    nDataSize,
                    pstContext->pBuffer,
                    pstContext->nBufferSize,
                    &nCbBuffer ) )
                {
                    // 이곳에서 NULL이 전달되면, sql statement 실행은 실패된다.
                    assert( 0 );
                    break;
                }

                pRtnValue = pstContext->pBuffer;
            }
            break;
        }


    } while (0);

    return pRtnValue;
}

void sqlite3FreeCodec( void* pContext )
{
    PCODEC_CONTEXT pstContext = NULL;

    if( pContext == NULL )
        return;

    do 
    {
        pstContext = (PCODEC_CONTEXT)pContext;
        if( pstContext == NULL )
            break;

        if( pstContext->pBuffer != NULL )
        {
            sqlite3_free( pstContext->pBuffer );
            pstContext->pBuffer = NULL;
        }

        if( pstContext->handle == NULL )
            break;

        pstContext->pfnSQLiteCodecDeInit( pstContext->handle );
        pstContext->handle = NULL;

    } while (0);

    sqlite3_free( pContext );
    pContext = NULL;
}

// Report the page size to the codec, called from pager.c (address passed in sqlite3PagerSetCodec)
void sqlite3CodecSizeChange( void *pCodec, int pageSize, int nReserve )
{
//     SetPageSize( pCodec, pageSize );
}

// called from
// ATTACH DATABASE db AS dba KEY k
// or
// PRAGMA KEY=k (sqlite_attach)
// zKey can be NULL.
// nKey,
//      minus - zKey as encrypt key. using abs(nKey) to get length.
//      0     - not encrypted
//      plus  - zKey as passphrase. using nKey to get length
int sqlite3CodecAttach( sqlite3* db, int nDb, const void* zKey, int nKey )
{
    int                         nRet = SQLITE_OK;
    unsigned int                nBlockSize = 0;
    struct Db*                  pDb = NULL;
    PCODEC_CONTEXT              pContext = NULL;

    do 
    {
        if( db == NULL || db->aDb == NULL )
        {
            nRet = SQLITE_INTERNAL;
            assert( 0 );
            break;
        }

        if( ( NULL == zKey ) && ( 928 == nKey ) )
        {
            // magic value.
            // already called sqlite3PagerSetCodec(...), so exit.
            nRet = SQLITE_OK;
            break;
        }

        if( ( zKey == NULL ) || ( nKey == 0) )
        {
            nRet = SQLITE_OK;
            assert( 0 );
            break;
        }

        // where is database?
        pDb = &db->aDb[ nDb ];

        if( ( pDb->pBt == NULL ) || ( sqlite3BtreePager( pDb->pBt ) == NULL ) )
        {
            nRet = SQLITE_INTERNAL;
            assert( 0 );
            break;
        }

        // allocation of internal context
        pContext = sqlite3MallocZero( sizeof( CODEC_CONTEXT ) );
        if( pContext == NULL )
        {
            nRet = SQLITE_NOMEM;
            assert( 0 );
            break;
        }

        // Assign codec information.
        // compiled with over Sqlite3 version 3006016.
        sqlite3PagerSetCodec( sqlite3BtreePager( pDb->pBt ), sqlite3Codec, sqlite3CodecSizeChange, sqlite3FreeCodec, (void*)pContext );

        pContext->eEncryptionType = GetSQLiteEncryptionType( zKey, nKey );
        SetSQLiteEncryptionHandler( pContext );

        // call external define function
         pContext->handle = pContext->pfnSQLiteCodecInit( zKey, abs( nKey ), &nBlockSize );
        if( pContext->handle == NULL )
        {
            nRet= SQLITE_NOMEM;
            assert( 0 );
            break;
        }

        if( sqlite3BtreeGetPageSize( pDb->pBt ) + 1 <= (int)nBlockSize )
        {
            // user defined block size check failed
            // block size must be smaller than page size
            nRet = SQLITE_INTERNAL;
            assert( 0 );
            break;
        }

        // configuration internal context
        pContext->pDB = pDb;
        pContext->pBt = pDb->pBt;
        pContext->unBlockSize = nBlockSize;
        pContext->unPageSize = sqlite3BtreeGetPageSize( pDb->pBt );
        pContext->unReserveSize = nBlockSize;
        pContext->nBufferSize = pContext->unPageSize;
        pContext->pBuffer = sqlite3MallocZero( pContext->nBufferSize );
        if( NULL == pContext->pBuffer )
        {
            nRet = SQLITE_NOMEM;
            assert( 0 );
            break;
        }

        // set page size.
        // pageSize : x^2. size of page to be save to file      ==> normally SQLITE_DEFAULT_PAGE_SIZE(1024 byte)
        // reserved : the user define area in the end of a page.==> block size byte
        //
        // actual page size to be saved is (pageSize-reserve).
        // we can reserve the block size of the codec algorithm,
        // encrypt a page can not be overflowed by increasing small block size.
        if( 0 != pContext->unReserveSize )
        {
            //////////////////////////////////////////////////////////////////////////
            // db lock
            sqlite3_mutex_enter( db->mutex );
            {
                sqlite3BtreeSetPageSize( pDb->pBt, pContext->unPageSize, pContext->unReserveSize, 0 );
            }
            sqlite3_mutex_leave( db->mutex );
            // db unlock
            //////////////////////////////////////////////////////////////////////////
        }

    } while ( 0 );

    return nRet;
}

TyEnSQLiteEncryptionType GetSQLiteEncryptionType( const void* zKey, int nKey )
{
    TyEnSQLiteEncryptionType eSQLiteEncryptionType = SQLITE_CODEC_NONE;
    char*                    pchBuffer = NULL;
    char                     szBuffer[ 4 ] = { 0, };
    char*                    pszBuffer = NULL;

    do 
    {
        if( zKey == NULL || nKey <= 0 )
            break;

        pchBuffer = malloc( nKey * (sizeof( char ) + 1) );
        memset( pchBuffer, '\0', nKey * ( sizeof( char ) + 1 ) );

#if defined( _MSC_VER )
        strncpy_s( pchBuffer, nKey + 1, (char*)zKey, _TRUNCATE );
#else
        strncpy( pchBuffer, (char*)zKey, nKey + 1 );
#endif

        // bf: 로 시작하거나 아무것도 없다면 blowfish 사용
        // a2: 로 시작하면 AES256
        // xo: 로 시작하면 XOR

        szBuffer[ 0 ] = pchBuffer[ 0 ];
        szBuffer[ 1 ] = pchBuffer[ 1 ];
        szBuffer[ 2 ] = pchBuffer[ 2 ];
        
        pszBuffer = _strlwr( szBuffer );

        if( pszBuffer[ 0 ] == 'a' && pszBuffer[ 1 ] == '2' && pszBuffer[ 2 ] == ':' )
        {
            eSQLiteEncryptionType = SQLITE_CODEC_AES256;
            break;
        }

        if( pszBuffer[ 0 ] == 'x' && pszBuffer[ 1 ] == 'o' && pszBuffer[ 2 ] == ':' )
        {
            eSQLiteEncryptionType = SQLITE_CODEC_XOR;
            break;
        }

        if( pszBuffer[ 0 ] == 'b' && pszBuffer[ 1 ] == 'f' && pszBuffer[ 2 ] == ':' )
        {
            eSQLiteEncryptionType = SQLITE_CODEC_BLOWFISH;
            break;
        }

        eSQLiteEncryptionType = SQLITE_CODEC_BLOWFISH;

    } while (0);

    if( pchBuffer != NULL )
        free( pchBuffer );

    return eSQLiteEncryptionType;
}

void SetSQLiteEncryptionHandler( PCODEC_CONTEXT pContext )
{
    switch( pContext->eEncryptionType )
    {
        case SQLITE_CODEC_XOR:
            pContext->pfnSQLiteCodecInit = SQLiteXORInit;
            pContext->pfnSQLiteCodecDeInit = SQLiteXORDeInit;
            pContext->pfnSQLiteCodecEncode = SQLiteXOREncode;
            pContext->pfnSQLiteCodecDecode = SQLiteXORDecode;
            break;
        case SQLITE_CODEC_BLOWFISH:
            pContext->pfnSQLiteCodecInit = SQLiteBlowfishInit;
            pContext->pfnSQLiteCodecDeInit = SQLiteBlowfishDeInit;
            pContext->pfnSQLiteCodecEncode = SQLiteBlowfishEncode;
            pContext->pfnSQLiteCodecDecode = SQLiteBlowfishDecode;
            break;
        case SQLITE_CODEC_AES256:
            pContext->pfnSQLiteCodecInit = SQLiteAES256Init;
            pContext->pfnSQLiteCodecDeInit = SQLiteAES256DeInit;
            pContext->pfnSQLiteCodecEncode = SQLiteAES256Encode;
            pContext->pfnSQLiteCodecDecode = SQLiteAES256Decode;
            break;
        default:
            assert( 0 );
            break;
    }
}

#else // SQLITE_HAS_CODEC
#pragma message("[codec.c] SQLITE_HAS_CODEC is not defined.")
#endif // SQLITE_HAS_CODEC
