CONFIG(release, debug|release):DEFINES *= NDEBUG
DEFINES += SQLITE_OMIT_LOAD_EXTENSION SQLITE_OMIT_COMPLETE
blackberry: DEFINES += SQLITE_ENABLE_FTS3 SQLITE_ENABLE_FTS3_PARENTHESIS SQLITE_ENABLE_RTREE
wince*: DEFINES += HAVE_LOCALTIME_S=0
INCLUDEPATH +=  $$PWD/sqlite
SOURCES += $$PWD/sqlite/alter.c \
    $$PWD/sqlite/analyze.c \
    $$PWD/sqlite/attach.c \
    $$PWD/sqlite/auth.c \
    $$PWD/sqlite/backup.c \
    $$PWD/sqlite/bitvec.c \
    $$PWD/sqlite/btmutex.c \
    $$PWD/sqlite/btree.c \
    $$PWD/sqlite/build.c \
    $$PWD/sqlite/callback.c \
    $$PWD/sqlite/complete.c \
    $$PWD/sqlite/ctime.c \
    $$PWD/sqlite/date.c \
    $$PWD/sqlite/dbstat.c \
    $$PWD/sqlite/delete.c \
    $$PWD/sqlite/expr.c \
    $$PWD/sqlite/fault.c \
    $$PWD/sqlite/fkey.c \
    $$PWD/sqlite/fts1.c \
    $$PWD/sqlite/fts1_hash.c \
    $$PWD/sqlite/fts1_porter.c \
    $$PWD/sqlite/fts1_tokenizer1.c \
    $$PWD/sqlite/fts2.c \
    $$PWD/sqlite/fts2_hash.c \
    $$PWD/sqlite/fts2_icu.c \
    $$PWD/sqlite/fts2_porter.c \
    $$PWD/sqlite/fts2_tokenizer.c \
    $$PWD/sqlite/fts2_tokenizer1.c \
    $$PWD/sqlite/fts3.c \
    $$PWD/sqlite/fts3_aux.c \
    $$PWD/sqlite/fts3_expr.c \
    $$PWD/sqlite/fts3_hash.c \
    $$PWD/sqlite/fts3_icu.c \
    $$PWD/sqlite/fts3_porter.c \
    $$PWD/sqlite/fts3_snippet.c \
    $$PWD/sqlite/fts3_tokenize_vtab.c \
    $$PWD/sqlite/fts3_tokenizer.c \
    $$PWD/sqlite/fts3_tokenizer1.c \
    $$PWD/sqlite/fts3_unicode.c \
    $$PWD/sqlite/fts3_unicode2.c \
    $$PWD/sqlite/fts3_write.c \
    $$PWD/sqlite/fts5.c \
    $$PWD/sqlite/func.c \
    $$PWD/sqlite/global.c \
    $$PWD/sqlite/hash.c \
    $$PWD/sqlite/icu.c \
    $$PWD/sqlite/insert.c \
    $$PWD/sqlite/json1.c \
    $$PWD/sqlite/legacy.c \
    $$PWD/sqlite/loadext.c \
    $$PWD/sqlite/main.c \
    $$PWD/sqlite/malloc.c \
    $$PWD/sqlite/mem0.c \
    $$PWD/sqlite/mem1.c \
    $$PWD/sqlite/mem2.c \
    $$PWD/sqlite/mem3.c \
    $$PWD/sqlite/mem5.c \
    $$PWD/sqlite/memjournal.c \
    $$PWD/sqlite/mutex.c \
    $$PWD/sqlite/mutex_noop.c \
    $$PWD/sqlite/mutex_unix.c \
    $$PWD/sqlite/mutex_w32.c \
    $$PWD/sqlite/notify.c \
    $$PWD/sqlite/opcodes.c \
    $$PWD/sqlite/os.c \
    $$PWD/sqlite/os_unix.c \
    $$PWD/sqlite/os_win.c \
    $$PWD/sqlite/pager.c \
    $$PWD/sqlite/parse.c \
    $$PWD/sqlite/pcache.c \
    $$PWD/sqlite/pcache1.c \
    $$PWD/sqlite/pragma.c \
    $$PWD/sqlite/prepare.c \
    $$PWD/sqlite/printf.c \
    $$PWD/sqlite/random.c \
    $$PWD/sqlite/resolve.c \
    $$PWD/sqlite/rowset.c \
    $$PWD/sqlite/rtree.c \
    $$PWD/sqlite/select.c \
    $$PWD/sqlite/sqlite3rbu.c \
    $$PWD/sqlite/sqlite3session.c \
    $$PWD/sqlite/status.c \
    $$PWD/sqlite/table.c \
    $$PWD/sqlite/threads.c \
    $$PWD/sqlite/tokenize.c \
    $$PWD/sqlite/treeview.c \
    $$PWD/sqlite/trigger.c \
    $$PWD/sqlite/update.c \
    $$PWD/sqlite/userauth.c \
    $$PWD/sqlite/utf.c \
    $$PWD/sqlite/util.c \
    $$PWD/sqlite/vacuum.c \
    $$PWD/sqlite/vdbe.c \
    $$PWD/sqlite/vdbeapi.c \
    $$PWD/sqlite/vdbeaux.c \
    $$PWD/sqlite/vdbeblob.c \
    $$PWD/sqlite/vdbemem.c \
    $$PWD/sqlite/vdbesort.c \
    $$PWD/sqlite/vdbetrace.c \
    $$PWD/sqlite/vtab.c \
    $$PWD/sqlite/wal.c \
    $$PWD/sqlite/walker.c \
    $$PWD/sqlite/where.c \
    $$PWD/sqlite/wherecode.c \
    $$PWD/sqlite/whereexpr.c
    
HEADERS += $$PWD/sqlite/sqlite3.h \
           $$PWD/sqlite/os.h \
           $$PWD/sqlite/sqliteint.h \
           $$PWD/sqlite/os_common.h

win32: HEADERS += $$PWD/sqlite/os_win.h
win32: HEADERS += $$PWD/sqlite/msvc.h

# loadable extension
SOURCES += $$PWD/sqlite/csv.c
SOURCES += $$PWD/sqlite/carray.c 

# crypto
SOURCES += $$PWD/sqlite/codec.c \
           $$PWD/sqlite/blowfish.cpp \
           $$PWD/sqlite/aes256.cpp \
           $$PWD/sqlite/aes256_base.cpp \
           $$PWD/sqlite/aes256_PRNG.cpp \
           $$PWD/sqlite/aes256_S_Box.cpp \
           $$PWD/sqlite/codec_aes256.cpp \
           $$PWD/sqlite/codec_blowfish.cpp \
           $$PWD/sqlite/codec_xor.cpp

HEADERS += $$PWD/sqlite/codec.h \
           $$PWD/sqlite/blowfish.h \
           $$PWD/sqlite/aes256.hpp \
           $$PWD/sqlite/aes256_base.hpp \
           $$PWD/sqlite/aes256_Byte_Block.hpp \
           $$PWD/sqlite/aes256_prng.hpp \
           $$PWD/sqlite/aes256_S_Box.hpp
           
# crypto 
DEFINES += SQLITE_HAS_CODEC

#additional
DEFINES += SQLITE_CORE 
DEFINES += SQLITE_LIKE_DOESNT_MATCH_BLOBS

DEFINES += SQLITE_ENABLE_DBSTAT_VTAB
DEFINES += SQLITE_ENABLE_FTS3 SQLITE_ENABLE_FTS3_PARENTHESIS SQLITE_ENABLE_STAT3
DEFINES += SQLITE_ENABLE_FTS4
DEFINES += SQLITE_ENABLE_FTS5
DEFINES += SQLITE_ENABLE_JSON1 
DEFINES += SQLITE_ENABLE_RTREE 
DEFINES += SQLITE_ENABLE_MEMSYS3
DEFINES += SQLITE_ENABLE_RBU

DEFINES += SQLITE_ENABLE_EXPLAIN_COMMENTS

DEFINES -= SQLITE_OMIT_LOAD_EXTENSION
DEFINES -= SQLITE_OMIT_COMPLETE
DEFINES += SQLITE_ENABLE_COLUMN_METADATA 
DEFINES += SQLITE_ENABLE_ATOMIC_WRITE 
DEFINES += SQLITE_ENABLE_UPDATE_DELETE_LIMIT  
DEFINES += SQLITE_ENABLE_UNLOCK_NOTIFY
DEFINES += SQLITE_ENABLE_MEMORY_MANAGEMENT 
DEFINES += SQLITE_SOUNDEX  
DEFINES += SQLITE_THREADSAFE=1
DEFINES += SQLITE_ENABLE_API_ARMOR

#log
DEFINES += SQLITE_ENABLE_STAT4
DEFINES += SQLITE_ENABLE_SQLLOG
SOURCES += $$PWD/sqlite/test_sqllog.c
