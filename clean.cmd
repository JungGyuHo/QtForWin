pushd %~dp0

mingw32-make distclean -j4
nmake distclean

if exist qtbase\bin\ (
    cd qtbase\bin
    del *.exe
    del *.manifest
    del *.exp
    del *.lib
    del *.pdb
    del *.dll
    cd ..
)

if exist tools\configure\ (
    cd tools\configure
    del *.manifest
    cd ..\..
)

if exist qmake\ (
    cd qmake
    mingw32-make distclean -j4
    nmake distclean
    del makefile
    del *.exe
    cd ..
)

if exist src\corelib\global\ (
    cd src\corelib\global
    del qconfig.h
    del qconfig.cpp
    cd ..\..\..
)

del configure.cache
del .qmake.cache
del config.summary

if exist lib\ (
    cd lib
    del /s /f *.prl
    del /s /f *.lib
    del /s /f *.res
    rd /s /q pkgconfig
    rd /s /q cmake
    cd ..
)

rd /s /q tmp
rd /s /q plugins
rd /s /q imports
rd /s /q include

if exist mkspecs\ (
    cd mkspecs
    rd /s /q default
    del qconfig.pri
    del qmodule.pri
    del qfeatures.pri

    if exist modules (
        cd modules
        del *.pri
        cd ..
    )

    if exist modules-inst (
        cd modules-inst
        del *.pri
        cd ..
    )
        
    cd ..
)

cd ..

del /s /f *.pch
del /s /f *.obj
del /s /f *.pdb
del /s /f *.tmp 
del /s /f makefile.debug
del /s /f makefile.release
del /s /f makefile
del /s /f *.sln
del /s /f *.vcproj
del /s /f *.vcxproj
del /s /f *.vcxproj.filters
del /s /f object_script.*.debug
del /s /f object_script.*.release
del /s /f Qt*resource.rc
del /s /f uic_wrapper.bat
del /s /f qdoc_wrapper.bat
del /s /f qhelpgenerator_wrapper.bat
del /s /f *_plugin_import.cpp

call deldirs tmp
call deldirs .moc
call deldirs .rcc
call deldirs .obj
call deldirs .uic
call deldirs .pch

if exist qtactiveqt\ (
    cd qtactiveqt
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    cd ..
)

if exist qtsvg\ (
    cd qtsvg
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    call ..\deldirs .uic
    call ..\deldirs .pch
    del /s /f *.lib
    del /s /f *.prl
    del /s /f uic_wrapper.bat
    del /s /f qdoc_wrapper.bat
    del /s /f qhelpgenerator_wrapper.bat
    del /s /f *_plugin_import.cpp
    cd ..
)

if exist qttools\ (
    cd qttools
    call ..\deldirs bin
    call ..\deldirs include
    call ..\deldirs mkspecs
    call ..\deldirs .moc
    call ..\deldirs .rcc
    call ..\deldirs .obj
    call ..\deldirs .uic
    call ..\deldirs .pch
    if exist lib\ (
        cd lib
        call ..\..\deldirs cmake
        del /s /f *.lib
        del /s /f *.prl
        cd..
    )
    del /s /f uic_wrapper.bat
    del /s /f qdoc_wrapper.bat
    del /s /f qhelpgenerator_wrapper.bat
    del /s /f *_plugin_import.cpp
    cd ..
)

if exist qttranslations\ (
    cd qttranslations
    call ..\deldirs debug
    call ..\deldirs release
    del /s /f *.qm
    del /s /f *_wrapper.bat
    cd ..
)

if exist qtxmlpatterns\ (
    cd qtxmlpatterns
    call ..\deldirs bin
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    call ..\deldirs .uic
    call ..\deldirs .pch
    del /s /f uic_wrapper.bat
    del /s /f qdoc_wrapper.bat
    del /s /f qhelpgenerator_wrapper.bat
    del /s /f *_plugin_import.cpp
    cd ..
)

if exist qtimageformats\ (
    cd qtimageformats
    call ..\deldirs lib
    call ..\deldirs mkspecs
    call ..\deldirs .uic
    call ..\deldirs .pch
    del /s /f uic_wrapper.bat
    del /s /f qdoc_wrapper.bat
    del /s /f qhelpgenerator_wrapper.bat
    del /s /f *_plugin_import.cpp
    del /s /f *.lib
    del /s /f *.prl
    cd ..
)

if exist qtscript\ (
    cd qtscript
    del /s /f *.lib
    del /s /f *.prl
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    call ..\deldirs .uic
    call ..\deldirs .pch
    del /s /f uic_wrapper.bat
    del /s /f qdoc_wrapper.bat
    del /s /f qhelpgenerator_wrapper.bat
    del /s /f *_plugin_import.cpp
    cd ..
)

if exist qtcompress\ (
    cd qtcompress
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    cd ..
)

if exist qtquazip\ (
    cd qtquazip
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    call ..\deldirs .moc
    call ..\deldirs .obj
    call ..\deldirs .pch
    cd ..
)

if exist qtftp\ (
    cd qtftp
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    call ..\deldirs .moc
    call ..\deldirs .obj
    call ..\deldirs .pch
    cd ..
)

if exist qthttp\ (
    cd qthttp
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    call ..\deldirs .moc
    call ..\deldirs .obj
    call ..\deldirs .pch
    cd ..
)

if exist qtremoteobjects\ (
    cd qtremoteobjects
    call ..\deldirs bin
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs .moc
    call ..\deldirs .obj
    call ..\deldirs .pch
    cd ..
)

if exist qtsystems\ (
    cd qtsystems
    call ..\deldirs bin
    call ..\deldirs lib
    call ..\deldirs include
    call ..\deldirs mkspecs
    call ..\deldirs .moc
    call ..\deldirs .obj
    call ..\deldirs .pch
    cd ..
)

cd qtbase

OriginalSlns.exe -y -gm2 -silent

popd

del .qmake.super
