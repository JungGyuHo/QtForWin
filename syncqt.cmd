if exist qtbase\ (
    cd qtbase
    perl bin\syncqt.pl -version 4.9.0 -module QtCore
    perl bin\syncqt.pl -version 4.9.0 -module QtConcurrent
    perl bin\syncqt.pl -version 4.9.0 -module QtDBus
    perl bin\syncqt.pl -version 4.9.0 -module QtGui
    perl bin\syncqt.pl -version 4.9.0 -module QtNetwork
    perl bin\syncqt.pl -version 4.9.0 -module QtOpenGL
    perl bin\syncqt.pl -version 4.9.0 -module QtSql
    perl bin\syncqt.pl -version 4.9.0 -module QtTest
    perl bin\syncqt.pl -version 4.9.0 -module QtWidgets
    perl bin\syncqt.pl -version 4.9.0 -module QtXml
    perl bin\syncqt.pl -version 4.9.0 -module QtZlib
    cd ..
)

if exist qtactiveqt\ (
    cd qtactiveqt
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module ActiveQt
    cd ..
)

if exist qtimageformats\ (
    cd qtimageformats
    cd ..
)

if exist qtscript\ (
    cd qtscript
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtScript
    cd ..
)

if exist qtsvg\ (
    cd qtsvg
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtSvg
    cd ..
)

if exist qtxmlpatterns\ (
    cd qtxmlpatterns
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtXmlPatterns
    cd ..
)

if exist qttools\ (
    cd qttools
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtCLucene
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtDesigner
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtDesignerComponents
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtHelp
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtUiTools
    cd ..
)

if exist qtcompress\ (
    cd qtcompress
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtCompress
    cd ..
)

if exist qtquazip\ (
    cd qtquazip
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtQuazip
    cd ..
)

if exist qtftp\ (
    cd qtftp
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtFtp
    cd ..
)

if exist qthttp\ (
    cd qthttp
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtHttp
    cd ..
)

if exist qtremoteobjects\ (
    cd qtremoteobjects
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtRemoteObjects
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtRepParser
    cd ..
)

if exist qtsystems\ (
    cd qtsystems
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtSystemInfo
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtPublishSubscribe
    perl ..\qtbase\bin\syncqt.pl -version 4.9.0 -module QtServiceFramework
    cd ..
)
