@ECHO OFF
@rem -opensource
@rem -confirm-license

PUSHD %~dp0

SET QT_VER=4.9.0
SET PREFIX_PATH= 
SET QT_STATIC=True
SET QT_STATIC_RUNTIME=True
SET QT_COMPILER=
SET QT_PLATFORM=X86

SET QT_CONFIGURE=
SET COMPILER_SET=
SET COMPILER_PATH=
SET QT_TARGET_XP=

IF "%1" == "/?" GOTO HELP
IF "%1" == "" GOTO HELP
IF "%2" == "" GOTO HELP
IF "%3" == "" GOTO HELP
IF "%4" == "" GOTO HELP

SET PREFIX_PATH=%1
SET QT_STATIC=%2
SET QT_STATIC_RUNTIME=%2
SET QT_COMPILER=%3
SET QT_PLATFORM=%4

IF /I "%QT_COMPILER%" == "VC2008" (
    SET COMPILER_SET=9.0)
IF /I "%QT_COMPILER%" == "VC2010" (
    SET COMPILER_SET=10.0)
IF /I "%QT_COMPILER%" == "VC2012" (
    SET COMPILER_SET=11.0)
IF /I "%QT_COMPILER%" == "VC2012_XP" (
    SET COMPILER_SET=11.0)
IF /I "%QT_COMPILER%" == "VC2013" (
    SET COMPILER_SET=12.0)
IF /I "%QT_COMPILER%" == "VC2013_XP" (
    SET COMPILER_SET=12.0)
IF /I "%QT_COMPILER%" == "VC2015" (
    SET COMPILER_SET=14.0)
IF /I "%QT_COMPILER%" == "VC2015_XP" (
    SET COMPILER_SET=14.0)
IF /I "%QT_COMPILER%" == "VC2017" (
    SET COMPILER_SET=14.1)
IF /I "%QT_COMPILER%" == "VC2017_XP" (
    SET COMPILER_SET=14.1)

SET INCLUDE=
SET LIB=
SET LINK=

ECHO Visual Studio Configuration...
SET COMPILER_PATH=C:\Program Files (x86)\Microsoft Visual Studio %COMPILER_SET%\VC\vcvarsall.bat
rem Call "%COMPILER_PATH%" %QT_PLATFORM%
Call "C:\Program Files (x86)\Microsoft Visual Studio %COMPILER_SET%\VC\vcvarsall.bat" %QT_PLATFORM% 

IF /I "%QT_COMPILER%" == "VC2012_XP" (
    Call "%~dp0\scripts\setVC_TargetXP.cmd" %QT_PLATFORM%
    SET QT_TARGET_XP=-target xp
)

IF /I "%QT_COMPILER%" == "VC2013_XP" (
    Call "%~dp0\scripts\setVC_TargetXP.cmd" %QT_PLATFORM%
    SET QT_TARGET_XP=-target xp
)

IF /I "%QT_COMPILER%" == "VC2015_XP" (
    Call "%~dp0\scripts\setVC_TargetXP.cmd" %QT_PLATFORM%
    SET QT_TARGET_XP=-target xp
)

REM 
SET LINK=

REM DXSDK
SET INCLUDE=%INCLUDE%;%DXSDK_DIR%include
SET LIB=%LIB%;%DXSDK_DIR%Lib\%QT_PLATFORM%
SET PATH=%DXSDK_DIR%Utilities\Bin\%QT_PLATFORM%;%PATH%

REM MYSQL
SET INCLUDE=%INCLUDE%;%~dp0\dbms\MySQL\include
SET LIB=%LIB%;%~dp0\dbms\MySQL\lib\%QT_PLATFORM%

SET QT_CONFIGURE=-opensource -confirm-license

IF /I "%QT_STATIC%" == "True" (

    SET QT_CONFIGURE=%QT_CONFIGURE% -prefix "%PREFIX_PATH%\Qt-%QT_VER%-%QT_COMPILER%-%QT_PLATFORM%-S-MT" -static -static-runtime

) ELSE (
    SET QT_CONFIGURE=%QT_CONFIGURE% -prefix "%PREFIX_PATH%\Qt-%QT_VER%-%QT_COMPILER%-%QT_PLATFORM%"

)

SET QT_CONFIGURE=%QT_CONFIGURE% -debug-and-release -force-debug-info -rtti -opengl desktop -nomake examples -nomake tests -mp -no-dbus -no-freetype
SET QT_CONFIGURE=%QT_CONFIGURE% -qt-sql-sqlite -plugin-sql-sqlite -qt-sql-odbc -plugin-sql-odbc
SET QT_CONFIGURE=%QT_CONFIGURE% -qt-sql-mysql -plugin-sql-mysql
SET QT_CONFIGURE=%QT_CONFIGURE% -qt-zlib -qt-pcre -no-icu -qt-libpng -qt-libjpeg -no-strip -no-directwrite -no-direct2d
SET QT_CONFIGURE=%QT_CONFIGURE% %QT_TARGET_XP%

ECHO %QT_CONFIGURE%

pause 
call configure %QT_CONFIGURE%
nmake && nmake install
GOTO END

:HELP
ECHO GYTNI QT Build Script
ECHO Build installPath isStatic buildCompiler buildPlatform
ECHO .
ECHO installPath = Qt installation path
ECHO isStatic = True,false 
ECHO buildCompiler = VC2008,VC2010,VC2012,VC2012_XP,VC2013,VC2013_XP,VC2015,VC2015_XP,VC2017,VC2017_XP
ECHO buildPlatform = X86,X64
ECHO EX) build F:\QtSrc\Build True VC2008 X86

:END
SET QT_VER=
SET QT_STATIC=
SET QT_STATIC_RUNTIME=
SET QT_COMPILER=
SET QT_PLATFORM=

SET QT_CONFIGURE=
SET COMPILER_SET=
SET COMPILER_PATH=
SET QT_TARGET_XP=

POPD
