﻿QT Version 

1. 4.9.0

SQLite

1. 컴파일 옵션 추가
2. AES256, Blowfish, Xor 암호화 추가

QString 

1. QString( const std::string& )
2. QString( const std::wstring& )
3. operator=( const std::string& )
4. operator=( const std::wstring& )
5. operator+=( const std::string& )
6. operator+=( const std::wstring& )

QVariant 

1. QVariant( const std::string& )
2. QVariant( const std::wstring& )
3. toStdString()
4. toStdWString()

QSysteTrayIcon - Windows

1. Windows 2K, XP, Vista 지원을 위한 코드 수정

QMYSQL Driver

1. qsql_mysql.pri 를 수정하여 정적 빌드일 때 MYSQL 정적 라이브러리를 사용하도록 변경

QtCompress

1.QtCompress 모듈 추가( https://github.com/nezticle/qtcompress )

QtQuazip

1. Quazip 모듈 추가( http://quazip.sourceforge.net/ )
2. Ver = 0.7.3

※ QtCompress 와 QtQuazip 은 QtBase 의 zlib 와 소스코드가 일치해야함. 

QtFtp & QtHttp

1. Qt5 에서 사라진 QtFtp 와 QtHttp 추가( from http://code.qt.io )

QtRemoteObject

1. 5.6.2 

QtSystems

1. 5.4.0

-- Backport 
MSVC: Fix installation of PDB files for static libraries
Optimized implementation of QReadWriteLock
QReadWriteLocker: Fix race in unlock


