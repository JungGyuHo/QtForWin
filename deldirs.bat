@echo off
SETLOCAL

SET PAT=%1

if not defined PAT GOTO :HELP
if "%PAT%" == "/?" GOTO :HELP

SET CNT=0

for /f "tokens=*" %%I in ('dir /b /s /ad %PAT%*') do if %%~nxI==%PAT% (
	rmdir /s /q "%%I"
	SET /A CNT=%CNT% + 1
)

if %CNT% GTR 0 (
	ECHO %CNT%개 디렉토리 삭제됨
)

ENDLOCAL
GOTO :EOF

:HELP
echo 모든 하위디렉토리에서 하나의 디렉터리를 삭제합니다.
echo.
echo DELDIRS 이름
GOTO :EOF

:EOF
